document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('input[id*="work-ignore"]').forEach(function (checkbox) {
        checkbox.addEventListener('click', function () {
            this.closest('.panel').classList.toggle('disable');
        });
        if (checkbox.checked) {
            checkbox.closest('.panel').classList.add('disable');
        }
    });

    document.querySelector(".ignore_all").addEventListener('click', function () {
        document.querySelectorAll("input[type=checkbox]").forEach(function (checkbox) {
            checkbox.checked = true;
            checkbox.closest('.panel').classList.add('disable');
        });
    });

    document.querySelector(".ignore_none").addEventListener('click', function () {
        document.querySelectorAll("input[type=checkbox]").forEach(function (checkbox) {
            checkbox.checked = false;
            checkbox.closest('.panel').classList.remove('disable');
        });
    });

    document.querySelector('#edit-submit').addEventListener('click', function (e) {
        let validate = [];
        document.querySelectorAll('input[id*="work-ignore"]').forEach(function (input) {
            if (!input.checked) {
                let panelBody = input.closest('.panel-body');
                if (panelBody && !panelBody.querySelector('.own')) {
                    let selected = [];
                    panelBody.querySelectorAll('input:checked').forEach(function (checkedInput) {
                        selected.push(checkedInput.name);
                    });
                    validate.push(selected.length ? "ok" : "error");
                    if (!selected.length) {
                        input.closest('.panel').classList.add('error');
                    }
                    else {
                        input.closest('.panel').classList.remove('error');
                    }
                }
            }
        });

        if (validate.includes("error")) {
            alert('A contributor must be selected for each publication not set to be ignored.');
            e.preventDefault();
            return false;
        }
    });
});
