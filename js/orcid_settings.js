(function ($, Drupal) {
  Drupal.behaviors.toggleImportsVisibility = {
    attach: function (context, settings) {
      var $checkbox = $('input[data-drupal-selector="edit-orcid-sync-authors"]', context);

      function toggleImports() {
        if ($checkbox.is(':checked')) {
          $('.imports').hide();
        } else {
          $('.imports').show();
        }
      }

      toggleImports();

      $checkbox.on('change', function () {
        toggleImports();
      });
    }
  };
})(jQuery, Drupal);
