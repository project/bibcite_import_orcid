<?php

namespace Drupal\bibcite_import_orcid\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;

/**
 * Provides a block with a ORCID SYNC button.
 *
 * @Block(
 *   id = "bibcite_import_orcid_block",
 *   admin_label = @Translation("ORCID SYNC Button"),
 * )
 */
class OrcidBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $hasOrcid = \Drupal::routeMatch()->getParameter('user')?->get('field_orcid')->value;
    $uid = \Drupal::routeMatch()->getParameter('user')?->id();
    if ($hasOrcid) {
      $url = Url::fromRoute('bibcite_import_orcid.import_bio', ['uid' => $uid]);
      $link = Link::fromTextAndUrl('Import Biography from ORCID', $url);
      $button = $link->toRenderable();
      $button['#attached']['library'][] = 'bibcite_import_orcid/orcid-import-block';
      return [
        'import_button' => $button,
        'import_form' => \Drupal::formBuilder()->getForm('Drupal\bibcite_import_orcid\Form\PrepareImport'),
      ];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['orcid_block_settings'] = $form_state->getValue('orcid_block_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    $user = \Drupal::routeMatch()->getParameter('user');
    if ($user && $user->get('field_orcid')->value) {
      return AccessResult::allowedIfHasPermission($account, 'view orcid import block');
    }
    return AccessResult::forbidden();
  }

}
