<?php

namespace Drupal\bibcite_import_orcid;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Import ORCID data.
 */
class Fetch {

  /**
   * Get user details from ORCID Public API.
   *
   * @return array
   *   The user details.
   */
  public function getUserFromOrcid($orcid) {
    $url = "https://pub.orcid.org/v3.0/{$orcid}";
    $client = new Client();
    try {
      $response = $client->get($url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);
      $user = json_decode($response->getBody(), TRUE);
      return $user;
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addStatus(t('getUserFromOrcid Exception:') . ' ' . $e->getMessage());
      return NULL;
    }
  }

  /**
   * Get the all user's works from ORCID Public API.
   *
   * @return array
   *   The user's works.
   */
  public function getAllWorksFromOrcid($orcid) {
    $url = "https://pub.orcid.org/v3.0/{$orcid}/works";
    $client = new Client();
    try {
      $response = $client->get($url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);
      $works = json_decode($response->getBody(), TRUE);

      return $works['group'];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addStatus(t('getAllWorksFromOrcid Exception:') . ' ' . $e->getMessage());
      return NULL;
    }
  }

  /**
   * Get a publications details from the ORCID Public API.
   *
   * @return array
   *   The publication details.
   */
  public function getOrcidWorkData($work_put_code, $orcid) {
    $url = "https://pub.orcid.org/v3.0/{$orcid}/works/{$work_put_code}";
    $client = new Client();
    try {
      $response = $client->get($url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);
      $work_data = json_decode($response->getBody(), TRUE);
      return $work_data["bulk"][0]["work"];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addStatus(t('getOrcidWorkData Exception:') . ' ' . $e->getMessage());
      return NULL;
    }
  }

  /**
   * Get the user's bio from the ORCID Public API.
   */
  public function syncBio($user) {
    $config = \Drupal::config('bibcite_import_orcid.settings');
    if ($config->get('orcid_fetch_bio')) {
      $last_sync = $user->get('field_last_sync')->value;
      $periodicity = $user->get('field_periodicity')->value;
      $now = new \DateTime();
      $last_sync_date = new \DateTime($last_sync);
      $diff = $now->diff($last_sync_date);
      $diff_days = $diff->days;
      if ((!$last_sync || $periodicity == 'monthly' && $diff_days > 30 && $now->format('j') === '1') || ($periodicity == '90days' && $diff_days > 90) || ($periodicity == 'annual' && $diff_days > 365)) {
        \Drupal::service('bibcite_import_orcid.import_user')->importUser($user->id());
      }
    }
  }

  /**
   * Maps the ORCID reference types to Bibcite reference types.
   *
   * @return string
   *   The Bibcite reference type.
   */
  public function bibciteEntityTypes($entity) {
    $pubs = [
      "annotation" => "miscellaneous",
      "artistic-performance" => "miscellaneous",
      "book-chapter" => "book_chapter",
      "book-review" => "miscellaneous",
      "book" => "book",
      "conference-abstract" => "conference_proceedings",
      "conference-paper" => "conference_paper",
      "conference-poster" => "miscellaneous",
      "data-set" => "database",
      "dictionary-entry" => "miscellaneous",
      "disclosure" => "miscellaneous",
      "dissertation-thesis" => "thesis",
      "edited-book" => "book",
      "encyclopedia-entry" => "miscellaneous",
      "invention" => "miscellaneous",
      "journal-article" => "journal_article",
      "journal-issue" => "journal",
      "lecture-speech" => "presentation",
      "license" => "miscellaneous",
      "magazine-article" => "magazine_article",
      "manual" => "miscellaneous",
      "newsletter-article" => "miscellaneous",
      "newspaper-article" => "newspaper_article",
      "online-resource" => "website",
      "other" => "miscellaneous",
      "patent" => "patent",
      "physical-object" => "miscellaneous",
      "presentation" => "presentation",
      "preprint" => "preprint",
      "registered-copyright" => "miscellaneous",
      "report" => "report",
      "research-technique" => "miscellaneous",
      "research-tool" => "miscellaneous",
      "software" => "software",
      "spin-off-company" => "miscellaneous",
      "standards-and-policy" => "miscellaneous",
      "supervised-student-publication" => "miscellaneous",
      "technical-standard" => "miscellaneous",
      "test" => "miscellaneous",
      "trademark" => "miscellaneous",
      "translation" => "miscellaneous",
      "website" => "website",
      "working-paper" => "miscellaneous",
      "undefined" => "miscellaneous",
    ];
    if (array_key_exists($entity, $pubs)) {
      return $pubs[$entity];
    }
    else {
      return "miscellaneous";
    }
  }

}
