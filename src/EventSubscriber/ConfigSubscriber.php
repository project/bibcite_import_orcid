<?php

namespace Drupal\bibcite_import_orcid\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts to configuration events.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * Reacts to the ConfigEvents::SAVE event.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The configuration event.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    if ($event->getConfig()->getName() == 'bibcite_import_orcid.settings') {
      $values = $event->getConfig()->getRawData();
      /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity_display */
      $entity_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('user.user.default');
      // orcid_sync_authors > field_author else field_references.
      if ($values['orcid_sync_authors'] == 0) {
        if ($entity_display->getComponent('field_author') != NULL) {
          $entity_display->removeComponent('field_author');
          $entity_display->setComponent('field_references', [
            'type' => 'entity_reference_autocomplete',
            'weight' => 20,
            'settings' => [
              'match_operator' => 'CONTAINS',
              'size' => '60',
              'placeholder' => '',
            ],
            'third_party_settings' => [],
          ]);
        }
      }
      else {
        $entity_display->removeComponent('field_references');
        $entity_display->setComponent('field_author', [
          'type' => 'entity_reference_autocomplete',
          'weight' => 20,
          'settings' => [
            'match_operator' => 'CONTAINS',
            'size' => '60',
            'placeholder' => '',
          ],
          'third_party_settings' => [],
        ]);
      }
      // orcid_fetch_bio > field_periodicity.
      if (isset($values['orcid_fetch_bio']) && $values['orcid_fetch_bio'] == 0) {
        if ($entity_display->getComponent('field_periodicity') != NULL) {
          $entity_display->removeComponent('field_periodicity');
        }
      }
      else {
        $entity_display->setComponent('field_periodicity', [
          'type' => 'options_select',
          'weight' => 20,
          'settings' => [
            'size' => '60',
            'placeholder' => '',
          ],
          'third_party_settings' => [],
        ]);
      }
      $entity_display->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onConfigSave'];
    return $events;
  }

}
