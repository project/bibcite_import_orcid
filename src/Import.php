<?php

namespace Drupal\bibcite_import_orcid;

use Drupal\bibcite\HumanNameParser;
use Drupal\user\Entity\User;

/**
 * Import ORCID data.
 */
class Import {
  /**
   * Process.
   *
   * @var \Drupal\bibcite_import_orcid\Process
   */
  protected $process;

  /**
   * Construct.
   */
  public function __construct() {
    $this->process = new Process();
  }

  /**
   * Import Publications.
   *
   * @return array
   *   The context with created or updated publications.
   */
  public function importOrcid($work, $uid, &$context) {
    $reference = \Drupal::entityTypeManager()->getStorage('bibcite_reference');
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
    $work_details = is_array($work) ? json_decode($work["pub"], TRUE) : json_decode($work, TRUE);
    $pub_id = is_array($work_details) && array_key_exists('bibcite_doi', $work_details) ? $this->process->getExistingPublication($work_details['title'], $work_details['type'], $work_details["bibcite_doi"]) : $this->process->getExistingPublication($work_details['title'], $work_details['type']);
    $pub_authors = [];
    // If new publication, create it.
    if (empty($pub_id)) {
      $bibcite_reference = $reference->create($work_details);
      $context['results']['create'][] = $work_details['title'];
      // Set unublished if setting is enabled.
      $default_status = \Drupal::config('bibcite_import_orcid.settings')->get('orcid_unpub_default');
      $default_status ? $bibcite_reference->status = 0 : NULL;
    }
    else {
      // Load existing.
      $bibcite_reference = $reference->load($pub_id);
      $context['results']['update'][] = $work_details['title'];
      $authors = $bibcite_reference->author->getValue();
      $linkedAuthorNames = $this->getLinkedAuthorNames($authors);
      $authorsArray = array_column($authors, 'target_id');
      $keysToIgnore = ['contribs_atts', 'user_authors', 'contribs', 'bibcite_authors'];
      foreach ($work_details as $key => $value) {
        if (isset($value) && !in_array($key, $keysToIgnore)) {
          $bibcite_reference->$key->value = $value;
        }
      }
    }
    // Check if the user is syncing authors.
    if (\Drupal::config('bibcite_import_orcid.settings')->get('orcid_sync_authors')) {
      $selected_author = $work["work"]["authors"];
      if (isset($selected_author) && $work_details["contribs"]) {
        foreach ($work_details["contribs"] as $key => $value) {
          // At least 2 words and 4 characters.
          if (strlen($value) < 5 || !str_contains($value, ' ')) {
            continue;
          }
          if ($key == $selected_author) {
            $id = $this->getIdFromUser($value, $user) ?? !empty(trim($value)) ? $this->createContributor($value) : NULL;
            $this->addContributorToUser($user, $id);
          }
          else {
            $cids = $this->searchContributor($value);
            if (!empty($pub_id) && is_array($cids) && is_array($authorsArray) && $existing = reset(array_intersect($cids, $authorsArray))) {
              $id = $existing;
            }
            else {
              $id = $this->findCommonWordKey($value, $linkedAuthorNames) ?? $this->createContributor($value);
            }
          }
          $pub_authors[] = $id;
          // Make sure all connected authors are in the publication.
          foreach ($linkedAuthorNames as $key => $name) {
            if (!in_array($key, $pub_authors)) {
              $pub_authors[] = $key;
            }
          }
          $pub_authors = array_unique($pub_authors);
        }
      }
    }
    else {
      if ($work_details["contribs"]) {
        if (empty($pub_id)) {
          $pub_authors = [];
          foreach ($work_details["contribs"] as $key => $value) {
            if (strlen($value) > 4 && str_contains($value, ' ')) {
              $pub_authors[] = $this->createContributor($value);
            }
          }
        }
        else {
          $pub_authors = $this->getContributorsFromRef($work_details["contribs"], $bibcite_reference);
        }
      }
    }
    $pub_authors ? $bibcite_reference->author = $pub_authors : NULL;
    $bibcite_reference->save();
    $pub_id = $bibcite_reference->id();
    if (!\Drupal::config('bibcite_import_orcid.settings')->get('orcid_sync_authors')) {
      $this->addPublicationToUser($user, $pub_id);
    }
    return $context;
  }

  /**
   * Search for a contributor.
   *
   * @param string $name
   *   The contibutor name.
   *
   * @return array|null
   *   The contributor IDs if found.
   */
  public function searchContributor(string $name) {
    $humanNameParser = new HumanNameParser();
    // Parse does not support more than 1 comma.
    $name_parts = explode(',', $name);
    if (count($name_parts) > 2) {
      return;
    }
    $parsed = $humanNameParser->parse(trim($name));

    // Check if the contributor exists.
    $cids = \Drupal::entityQuery('bibcite_contributor')
      ->accessCheck(FALSE)
      ->condition('first_name', $parsed['first_name'])
      ->condition('last_name', $parsed['last_name'])
      ->execute();
    return $cids ?? NULL;
  }

  /**
   * Given a author name and $uid, look for a contributor ID.
   *
   * @param string $name
   *   The contibutor name.
   * @param \Drupal\user\Entity\User $user
   *   The user object.
   *
   * @return int|null
   *   The the contributor ID if found.
   */
  public function getIdFromUser(string $name, User $user) {
    if ($cids = $this->searchContributor($name)) {
      $contributors = array_column($user->field_author->getValue(), 'target_id');
      if ($userCids = array_intersect($cids, $contributors)) {
        return reset($userCids);
      }
    }
  }

  /**
   * Add a publication to a user.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user object.
   * @param int $pub_id
   *   Reference ID.
   *
   * @return void
   *   Add the publication to the user.
   */
  public function addPublicationToUser(User $user, int $pub_id) {
    $user_references = $user->field_references->getValue();
    $current_references = isset($user_references) ? array_column($user_references, 'target_id') : [];
    if (!in_array($pub_id, $current_references)) {
      $user->field_references[] = ['target_id' => $pub_id];
      $user->save();
    }
  }

  /**
   * Add a contributor to a user.
   */
  public function addContributorToUser(User $user, int $contrib_id) {
    $user_authors = $user->field_author->getValue();
    $current_authors = isset($user_authors) ? array_column($user_authors, 'target_id') : [];
    if (!in_array($contrib_id, $current_authors)) {
      $user->field_author[] = ['target_id' => $contrib_id];
      $user->save();
    }
  }

  /**
   * Compare authors and contributors.
   *
   * @param array $authors
   *   The authors from the ORCID work.
   * @param \Drupal\bibcite_entity\Entity\Reference $bibcite_reference
   *   The reference entity.
   *
   * @return array
   *   Return the consolidated contributors ids.
   */
  public function getContributorsFromRef(array $authors, $bibcite_reference) {
    $existing_contribs = $bibcite_reference->author->getValue();
    $existing_contribs = array_column($existing_contribs, 'target_id');
    $contributor_ids = [];
    foreach ($authors as $author) {
      // At least 2 words and 4 characters.
      if (strlen($author) < 5 || !str_contains($author, ' ')) {
        continue;
      }
      if ($existing_ids = $this->searchContributor($author)) {
        $common_ids = array_intersect($existing_ids, $existing_contribs);
        $contributor_ids[] = $common_ids ? reset($common_ids) : $this->createContributor($author);
      }
      else {
        $contributor_ids[] = $this->createContributor($author);
      }
    }
    return $contributor_ids;
  }

  /**
   * Create an author and retutn the id.
   *
   * @return int|null
   *   Return the id of the author.
   */
  public function createContributor($authorName) {
    $contributor = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    $humanNameParser = new HumanNameParser();
    // Parse does not support more than 1 comma.
    $name_parts = explode(',', $authorName);
    if (count($name_parts) > 2) {
      return;
    }
    $parsed = $humanNameParser->parse($authorName);
    $parsed['type'] = 'entity_contributor';
    $c = $contributor->create($parsed);
    $c->save();
    $id = $c->id();
    return $id;
  }

  /**
   * Import publications for all users.
   *
   * @return void
   *   Import publications.
   */
  public static function importUsersPublications() {
    // Start with users that don't have any pubs yet.
    $uidsWithoutPublications = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('field_orcid', '', '!=')
      ->condition('status', 1)
      ->notExists('field_references')
      ->execute();

    $uidsWithPublications = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('field_orcid', '', '!=')
      ->condition('status', 1)
      ->exists('field_references')
      ->execute();

    $uids = array_merge($uidsWithoutPublications, $uidsWithPublications);

    $user = \Drupal::entityTypeManager()->getStorage('user');

    foreach ($uids as $uid) {
      $user = $user->load($uid);
      $orcid = $user->field_orcid->value;
      $fetch = new Fetch();
      $works = $fetch->getAllWorksFromOrcid($orcid);
      $process = new Process();
      $context = [];
      foreach ($works as $work) {
        $logArray = $process->prepImportOrcid($work, $user, $context);
        if (is_array($logArray['results']) && !empty($logArray['results'])) {
          $op = array_key_first($logArray['results']);
          $message = $op . ': ' . end($logArray['results'][$op]);
          \Drupal::logger('bibcite_import_orcid')->notice($message);
        }
      }
    }
  }

  /**
   * Import user bio for all users.
   *
   * @return void
   *   Import users bio.
   */
  public static function importUsersBio() {
    $query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('field_orcid', '', '!=')
      ->condition('status', 1);
    $uids = $query->execute();
    foreach ($uids as $uid) {
      \Drupal::service('bibcite_import_orcid.import_user')->importUser($uid);
    }
  }

  /**
   * Get Linked Author Names.
   *
   * @param array $authors
   *   The authors from the ORCID work.
   *
   * @return array
   *   Return the linked author names.
   */
  public function getLinkedAuthorNames(array $authors) {
    $names = [];
    $contributor = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    foreach ($authors as $authorId) {
      $authorUids = \Drupal::entityQuery('user')
        ->accessCheck(FALSE)
        ->condition('field_author', $authorId['target_id'], 'IN')
        ->execute();
      if ($authorUids) {
        $author = $contributor->load($authorId['target_id']);
        $names[$authorId['target_id']] = $author->label();
      }
      else {
        continue;
      }
    }
    return $names;
  }

  /**
   * Find common word key.
   *
   * @param string $value
   *   The value to search.
   * @param array $linkedAuthorNames
   *   The linked author names.
   *
   * @return int|null
   *   Return the key of the common word.
   */
  public function findCommonWordKey($value, $linkedAuthorNames) {
    $valueWords = explode(' ', $value);
    foreach ($linkedAuthorNames as $key => $name) {
      $nameWords = explode(' ', $name);
      foreach ($valueWords as $valueWord) {
        $valueWord = str_replace(',', '', $valueWord);
        if (in_array($valueWord, $nameWords)) {
          return $key;
        }
      }
    }
    return NULL;
  }

}
