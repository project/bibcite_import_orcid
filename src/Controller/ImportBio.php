<?php

namespace Drupal\bibcite_import_orcid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for the import bio page.
 */
class ImportBio extends ControllerBase {

  /**
   * Import the user's biography from ORCID.
   */
  public function build($uid) {
    // Call the service to import the user's biography.
    \Drupal::service('bibcite_import_orcid.import_user')->importUser($uid);

    $url = Url::fromRoute('entity.user.canonical', ['user' => $uid]);
    $response = new RedirectResponse($url->toString());

    \Drupal::messenger()->addMessage($this->t('The biography has been imported.'));

    return $response;
  }

}
