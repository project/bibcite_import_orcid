<?php

namespace Drupal\bibcite_import_orcid\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for ORCID Import routes.
 */
class OrcidImportSuccessPageController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build($id) {
    $updated_works = $_SESSION['results_orcid_' . $id]['update'] ?? $_SESSION['works_data_' . $id][0]["results"]["update"] ?? [];
    $created_works = $_SESSION['results_orcid_' . $id]['create'] ?? $_SESSION['works_data_' . $id][0]["results"]["create"] ?? [];

    $markup = "<h2>Results of ORCID import:</h2>";
    $markup .= "<div>";
    if (count($updated_works) > 0) {
      $markup .= "<h2 class='works-resume'>" . count($updated_works) . " references updated.</h2>";

      $markup .= "<table border='0' id='table-works'><tr><th>Title</th></tr>";
      foreach ($updated_works as $element) {
        $markup .= "<tr><td>" . $element . "</td></tr>";
      }
      $markup .= "</table>";
      $markup .= "<br><br>";
    }
    if (count($created_works) > 0) {
      $markup .= "<h2 class='works-resume'>" . count($created_works) . " references created.</h2>";

      $markup .= "<table border='0' id='table-works'><tr><th>Title</th></tr>";
      foreach ($created_works as $element) {
        $markup .= "<tr><td>" . $element . "</td></tr>";
      }
      $markup .= "</table>";
      $markup .= "<br><br>";
    }

    $markup .= "<a id='btn-return-profile' href='/user/" . $_SESSION["uid"] . "'>Return to profile</a>";
    $markup .= "<br><br>";

    $markup .= "</div>";

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $markup,
      "#prefix" => '<div class="container">',
      "#suffix" => '</div>',
      '#attached' => [
        'library' => [
          'bibcite_import_orcid/orcid-import-block',
        ],
      ],
    ];

    return $build;
  }

}
