<?php

namespace Drupal\bibcite_import_orcid\Controller;

use Drupal\bibcite_entity\Entity\Reference;
use Drupal\Core\Controller\ControllerBase;

/**
 * Creates a page with the results of the import.
 */
class OrcidImporterPageController extends ControllerBase {

  /**
   * Maps the ORCID entity types to the Bibcite entity types.
   *
   * @var array
   * */
  public $bibciteEntityTypes = [
    "annotation" => "miscellaneous",
    "artistic-performance" => "miscellaneous",
    "book-chapter" => "book_chapter",
    "book-review" => "miscellaneous",
    "book" => "book",
    "conference-abstract" => "conference_proceedings",
    "conference-paper" => "conference_paper",
    "conference-poster" => "miscellaneous",
    "data-set" => "database",
    "dictionary-entry" => "miscellaneous",
    "disclosure" => "miscellaneous",
    "dissertation-thesis" => "thesis",
    "edited-book" => "book",
    "encyclopedia-entry" => "miscellaneous",
    "invention" => "miscellaneous",
    "journal-article" => "journal_article",
    "journal-issue" => "journal",
    "lecture-speech" => "presentation",
    "license" => "miscellaneous",
    "magazine-article" => "magazine_article",
    "manual" => "miscellaneous",
    "newsletter-article" => "miscellaneous",
    "newspaper-article" => "newspaper_article",
    "online-resource" => "website",
    "other" => "miscellaneous",
    "patent" => "patent",
    "physical-object" => "miscellaneous",
    "presentation" => "presentation",
    "preprint" => "preprint",
    "registered-copyright" => "miscellaneous",
    "report" => "report",
    "research-technique" => "miscellaneous",
    "research-tool" => "miscellaneous",
    "software" => "software",
    "spin-off-company" => "miscellaneous",
    "standards-and-policy" => "miscellaneous",
    "supervised-student-publication" => "miscellaneous",
    "technical-standard" => "miscellaneous",
    "test" => "miscellaneous",
    "trademark" => "miscellaneous",
    "translation" => "miscellaneous",
    "website" => "website",
    "working-paper" => "miscellaneous",
    "undefined" => "miscellaneous",
  ];

  /**
   * Creates a page with the results of the import.
   */
  public function content($user_orcid_id) {

    $result = \Drupal::service("bibcite_import_orcid.importer")->importOrcidData($user_orcid_id);

    $markup = $this->generateMarkup($result, $user_orcid_id);

    return $markup;
  }

  /**
   * Generates the markup for the page.
   */
  public function generateMarkup($result, $user_orcid_id) {

    $markup = "<div class='container'>";
    $markup .= "<h2>Resultado da importação para o ORCID " . $user_orcid_id . ":</h2>";
    $markup .= "<div>";

    if (intval($result['updated']) > 0) {
      $markup .= "<p> Foram actualizadas " . $result['updated'] . " referências.</p>";
    }

    if (intval($result['created']) > 0) {
      $markup .= "<p> Foram criadas " . $result['created'] . " referências.</p>";
    }

    if (count($result['entities']) > 0) {

      $markup .= "<ul>";

      foreach ($result['entities'] as $ref_id) {
        $ref_entity = Reference::load($ref_id);
        $markup .= "<li><a href='/bibcite/reference/" . $ref_id . "'>" . $ref_entity->get('title')->getValue()[0]['value'] . "</a></li>";
      }

      $markup .= "</ul>";
    }
    $markup .= "</div>";
    $markup .= "</div>";

    return [
      '#markup' => $markup,
    ];
  }

}
