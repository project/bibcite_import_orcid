<?php

namespace Drupal\bibcite_import_orcid\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines bibcite_import_orcid configuration form.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bibcite_import_orcid_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bibcite_import_orcid.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'bibcite_import_orcid/orcid-settings';
    $config = $this->config('bibcite_import_orcid.settings');

    $form['orcid_instructions'] = [
      "#markup" => $this->t("Customize how your ORCID integration should work") . "<br><br>",
    ];

    $form['orcid_unpub_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import publications as unpublished'),
      '#default_value' => $config->get('orcid_unpub_default'),
    ];
    $form['orcid_sync_authors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync authors from ORCID'),
      '#default_value' => $config->get('orcid_sync_authors') ?? 1,
    ];
    $form['orcid_sync_frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Sync Frequency'),
      '#default_value' => $config->get('orcid_sync_frequency') ?? 'none',
      '#options' => [
        'none' => $this->t('None'),
        'daily' => $this->t('Daily'),
        'weekly' => $this->t('Weekly'),
        'monthly' => $this->t('Monthly'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="orcid_sync_authors"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['orcid_fetch_bio'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync bios from ORCID'),
      '#default_value' => $config->get('orcid_fetch_bio') ?? 0,
    ];
    $orcid_sync_authors_disabled = $config->get('orcid_sync_authors') == 0;
    if ($orcid_sync_authors_disabled) {
      $form['import_buttons'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['imports']],
      ];

      $form['import_buttons']['import_all_publications'] = [
        '#type' => 'link',
        '#title' => $this->t('Import all publications'),
        '#url' => Url::fromUri('internal:/orcid-import/import-all-works'),
        '#attributes' => ['class' => ['button']],
      ];

      $form['import_buttons']['import_all_bios'] = [
        '#type' => 'link',
        '#title' => $this->t('Import all bios'),
        '#url' => Url::fromUri('internal:/orcid-import/import-all-bios'),
        '#attributes' => ['class' => ['button']],
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('bibcite_import_orcid.settings')
      ->set('orcid_unpub_default', $values['orcid_unpub_default'])
      ->set('orcid_sync_authors', $values['orcid_sync_authors'])
      ->set('orcid_sync_frequency', $values['orcid_sync_frequency'])
      ->save();

    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}
