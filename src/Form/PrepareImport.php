<?php

namespace Drupal\bibcite_import_orcid\Form;

use Drupal\bibcite_import_orcid\Fetch;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a ORCID Prepare form.
 */
class PrepareImport extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prepare_import';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = ['#type' => 'submit', '#value' => t('Import publications from ORCID')];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::routeMatch()->getParameter('user');
    $uid = $user->id();
    $orcid = trim($user->get('field_orcid')->value);
    $fetch = new Fetch();
    $fetch->syncBio($user);
    $works = $fetch->getAllWorksFromOrcid($orcid);
    // Sync the user bio if needed.
    $config = \Drupal::config('bibcite_import_orcid.settings');

    $_SESSION['uid'] = $uid;

    $batch = [
      'title' => t('Fetching publications from ORCID, please wait...'),
      'operations' => [],
      'finished' => 'bibcite_prep_import_orcid_batch_finished',
      'init_message' => t('Preparing the form'),
      'progress_message' => t('@current of @total. Estimated duration: @estimate.'),
      'error_message' => t('There was an error on the import process.'),
      'file' => \Drupal::service('extension.list.module')->getPath('bibcite_import_orcid') . '/bibcite_import_orcid.batch.inc',
    ];
    foreach ($works as $work) {
      $batch['operations'][] = [
        'bibcite_prep_import_orcid_batch',
        [$work, $user],
      ];
    };
    batch_set($batch);
  }

}
