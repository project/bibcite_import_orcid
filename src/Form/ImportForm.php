<?php

namespace Drupal\bibcite_import_orcid\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;

/**
 * Provides a ORCID Import form.
 */
class ImportForm extends FormBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bibcite_import_orcid_import';
  }

  /**
   * Try to find the most similar author.
   *
   * @return int
   *   The most similar string key.
   */
  public function getSimilarStringKey(string $author, array $options) {
    $max_common_words = 0;
    $candidates = [];

    // Convert author to lowercase for case-insensitive comparison.
    $lower_author = strtolower($author);
    $author_words = explode(' ', $lower_author);

    foreach ($options as $key => $option) {
      $lower_option = strtolower($option);
      $lower_option = str_replace(',', '', $lower_option);
      $option_words = explode(' ', $lower_option);
      $common_words = count(array_intersect($author_words, $option_words));

      if ($common_words > $max_common_words) {
        $max_common_words = $common_words;
        $candidates = [$key];
      }
      elseif ($common_words == $max_common_words) {
        $candidates[] = $key;
      }
    }

    if (count($candidates) == 1) {
      return array_pop($candidates);
    }

    // Use Levenshtein distance for ties.
    $levenshtein_distance = PHP_INT_MAX;
    $similar_string_key = NULL;

    foreach ($candidates as $key) {
      $distance = levenshtein($lower_author, strtolower($options[$key]));
      if ($distance < $levenshtein_distance) {
        $levenshtein_distance = $distance;
        $similar_string_key = $key;
      }
    }

    return $similar_string_key;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $uid = $_SESSION['uid'];
    $works = $_SESSION['works_data_' . $id];
    if (!empty($works)) {
      $form['works'] = [
        "#tree" => TRUE,
        "#prefix" => t('<div class="container"><div class="ign_actions"><a class="ignore_all">Ignore all</a><a class="ignore_none">Ignore none</a></div>'),
        "#suffix" => '</div>',
      ];

      foreach ($works as $item) {
        $work = json_decode($item);
        $title = $work->title;
        $options = (array) $work->contribs;
        $user_ids = $work->user_authors;

        $element = [];

        $element['work'] = [
          '#type' => 'fieldset',
          '#title' => $title,
        ];

        $element['work']['ignore'] = [
          '#type' => 'checkbox',
          '#title' => t('Ignore this publication'),
        ];
        $element['reference_works'][] = [
          '#type' => 'textarea',
          '#value' => $item,
          '#attributes' => [
            'style' => ['display:none'],
          ],
        ];
        $pub_user = array_flip((array) $options);
        $com = array_intersect((array) $user_ids, $pub_user);
        if (count($com) > 0) {
          $element['work']['own'] = [
            '#type' => 'markup',
            '#markup' => t('<div class="own">Already linked to the current user. Only info will be updated.</div>'),
          ];
        }
        else {
          $user = User::load($uid);
          if ($user->field_first_name && $user->field_last_name) {
            $author = $user->field_first_name->value . ' ' . $user->field_last_name->value;
          }
          elseif (!empty($user->field_name)) {
            $author = $user->field_name->value;
          }
          $key = $this->getSimilarStringKey($author, $options);
          // !implode($options) && $options[0] = $author;
          $strong = '<strong>' . $options[$key] . '</strong>';
          $options[$key] = $strong;

          $element['work']['authors'] = [
            '#type' => 'radios',
            '#title' => t('Authors'),
            '#options' => $options,
          ];
          $attribs = $work->contribs_atts;
          if ($attribs) {
            $element['work']['authors']['#options_attributes'][key($attribs)] = [
              "disabled" => "disabled",
            ];
          }
        }

        $form['works'][] = $element;
      }

      // If there are no publications to show.
      if (empty($works)) {
        $form['#attached']['library'][] = "bibcite_import_orcid/orcid-import-block";
        $form['works'][] = [
          '#type' => 'text',
          "#prefix" => t('<div class="orcid-no-results">No publications to import.<br><a href="/user/') . $uid . t('">Return to the profile page</a></div>'),
        ];
      }
      else {
        $form['actions'] = [
          '#type' => 'actions',
        ];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Import'),
          "#prefix" => '<div class="container">',
          "#suffix" => '</div>',
        ];
      }
      $form['#attached']['library'][] = 'bibcite_import_orcid/orcid-import-block';
      return $form;
    }
    else {
      return new TrustedRedirectResponse('/user');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $works = $form_state->getValue('works');
    foreach ($works as $key => $value) {
      if ($value["work"]["ignore"] === 0 && isset($value["work"]["authors"]) && $value["work"]["authors"] === NULL) {
        $form_state->setErrorByName('authors', $this->t('A contributor must be selected for each publication not set to be ignored.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = $_SESSION['uid'];
    $all_values = $form_state->getValues();
    $form_works = $all_values['works'];

    // Array for the batch.
    $operations = [];

    foreach ($form_works as $key => $value) {
      // Cleanup, add to queue.
      if ($value['work']["ignore"] === 0) {
        $value['uid'] = $uid;
        $value['pub'] = $value['reference_works'][0];
        $value['author'] = $value['work']['authors'];
        $operations[] = ['bibcite_import_orcid_batch', [$value, $uid]];
      }
    }

    $batch = [
      'title' => t('Preparing import form, please wait...'),
      'operations' => $operations,
      'finished' => 'bibcite_import_orcid_batch_finished',
      'init_message' => t('Importing'),
      'progress_message' => t('@current of @total. Estimated duration: @estimate.'),
      'error_message' => t('There was an error on the import process.'),
      'file' => \Drupal::service('extension.list.module')->getPath('bibcite_import_orcid') . '/bibcite_import_orcid.batch.inc',
    ];

    batch_set($batch);
  }

}
