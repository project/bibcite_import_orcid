<?php

namespace Drupal\bibcite_import_orcid\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class OrcidImportCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OrcidImportCommands object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Deletes all references from bibcite.
   *
   * @command bibcite_import_orcid:delete_refs
   * @aliases ouda
   * @usage bibcite_import_orcid:delete_refs
   */
  public function deleteRefs() {
    $refs_to_delete = $this->entityTypeManager->getStorage('bibcite_reference')->getQuery()
      ->accessCheck(FALSE)
      ->execute();
    echo "Deleting " . count($refs_to_delete) . " References.\n";
    $storage_handler = $this->entityTypeManager->getStorage("bibcite_reference");
    $entities = $storage_handler->loadMultiple($refs_to_delete);
    $storage_handler->delete($entities);
  }

  /**
   * Deletes all contributors from bibcite.
   *
   * @command bibcite_import_orcid:delete_contribs
   * @aliases ouda
   * @usage bibcite_import_orcid:delete_contribs
   */
  public function deleteContribs() {
    $contribs_to_delete = $this->entityTypeManager->getStorage('bibcite_contributor')->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    echo "Deleting " . count($contribs_to_delete) . " Contributors.\n";

    $storage_handler = $this->entityTypeManager->getStorage("bibcite_contributor");
    $entities = $storage_handler->loadMultiple($contribs_to_delete);
    $storage_handler->delete($entities);
  }

  /**
   * Deletes all entities from bibcite.
   *
   * @command bibcite_import_orcid:delete_all
   * @aliases ouda
   * @usage bibcite_import_orcid:delete_all
   */
  public function deleteAll() {

    // Delete References.
    $this->deleteRefs();

    // Delete Contributors.
    $this->deleteContribs();

  }

  /**
   * Deletes all references to contributors from users.
   *
   * @command bibcite_import_orcid:delete_users_contribs
   * @aliases ouduc
   * @usage bibcite_import_orcid:delete_users_contribs
   */
  public function deleteUserContribs() {
    $uids = $this->entityTypeManager->getStorage('user')->getQuery()
      ->accessCheck(FALSE)
      ->exists('field_author')
      ->execute();
    $userStorage = $this->entityTypeManager->getStorage("bibcite_contributor");
    $users = $userStorage->loadMultiple($uids);
    foreach ($users as $user) {
      $user->field_author = [];
      $user->save();
    }
  }

}
