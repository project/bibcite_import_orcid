<?php

namespace Drupal\bibcite_import_orcid;

use Drupal\user\Entity\User;

/**
 * Import ORCID Bio.
 */
class ImportUserService {

  /**
   * Import user bio from ORCID.
   */
  public function importUser($uid) {
    $user = User::load($uid);
    $syncPeriodocity = $user->get('field_periodicity')->value;
    if ($syncPeriodocity == 'no') {
      return;
    }
    $orcid_id = $user->get('field_orcid')->value;
    $orcidFetcher = new Fetch();
    $userBio = $orcidFetcher->getUserFromOrcid($orcid_id);
    $bio = $userBio['person']['biography']['content'];
    $user->set('field_bio', $bio);
    $now = new \DateTime();
    $user->set('field_last_sync', $now->format('Y-m-d'));
    $user->save();
  }

}
