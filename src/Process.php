<?php

namespace Drupal\bibcite_import_orcid;

use Drupal\bibcite_entity\Entity\Reference;
use RenanBr\BibTexParser\Exception\ProcessorException;
use RenanBr\BibTexParser\Listener;
use RenanBr\BibTexParser\Parser;
use RenanBr\BibTexParser\Processor\NamesProcessor;
use RenanBr\BibTexParser\Processor\TagNameCaseProcessor;

/**
 * Process ORCID data.
 */
class Process {

  /**
   * Fetch.
   *
   * @var \Drupal\bibcite_import_orcid\Fetch
   */
  protected $fetch;

  /**
   * Construct.
   */
  public function __construct() {
    $this->fetch = new Fetch();
  }

  /**
   * Converts ORCID response into bibcite reference.
   *
   * @return array
   *   Works json encoded.
   */
  public function prepImportOrcid($work, $user, &$context) {
    $uid = $user->id();
    $orcid = trim($user->get('field_orcid')->value);
    $summary = $work['work-summary'][0];
    $orcid_type = $summary["type"];
    $title = substr($summary['title']['title']['value'], 0, 250);
    $work_put_code = $summary['put-code'];
    $type = $this->fetch->bibciteEntityTypes($orcid_type);
    $work_data = $this->fetch->getOrcidWorkData($work_put_code, $orcid);
    $bibtex = $work_data["citation"]["citation-value"] ?? NULL;
    $bibtex_info = !empty($bibtex) ? $this->parseBibtex($bibtex) : "";
    $contributors = $this->getOrcidAuthors($work_data, $orcid, $bibtex_info);
    if (\Drupal::config('bibcite_import_orcid.settings')->get('orcid_sync_authors')) {
      // Search for the reference in bibcite.
      $query_ref = $this->getExistingPublication($title, $type, $work_data['doi']);

      // If the publication is new, add an option for each contributor.
      if (!$query_ref) {
        foreach ($contributors as $contributor) {
          $options[] = $contributor;
        }
      }
      else {
        $ref_authors = $this->getReferenceAuthors($query_ref, $uid, $contributors);
        [$options, $attributes] = $ref_authors;

        // Get the user contributor ids.
        $user_authors_ids = array_column($user->field_author->getValue(), 'target_id', 'target_id');
      }
    }
    else {
      $options = $contributors;
    }
    // Prepare output.
    $sub_title = isset($work_data['journal-title']['value']) ? substr($work_data['journal-title']['value'] ?? '', 0, 250) : '';
    $reference_data = [
      'type' => $type,
      'title' => $title,
      'bibcite_secondary_title' => preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', $sub_title),
      'bibcite_year' => $summary['publication-date']['year']['value'],
      'bibcite_other_number' => $work_put_code,
      'bibcite_abst_e' => $work_data['short-description'],
      'contribs' => $options,
      'user_authors' => $user_authors_ids ?? NULL,
      'contribs_atts' => $attributes ?? NULL,
      'uid' => $uid,
    ];
    // If $bibtex_info, merge into $reference_data.
    if ($bibtex_info) {
      $reference_data = array_merge($reference_data, $bibtex_info);
    }
    $work_ext_references = $work_data['external-ids']['external-id'];

    // Add more references if available.
    if (!empty($work_ext_references)) {
      foreach ($work_ext_references as $ext_ref) {
        switch ($ext_ref['external-id-type']) {
          case 'doi':
            $reference_data['bibcite_url'] = $ext_ref['external-id-url'];
            $reference_data['bibcite_doi'] = $ext_ref['external-id-value'];
            break;

          case 'issn':
            $reference_data['bibcite_issn'] = $ext_ref['external-id-value'];
            break;

          case 'isbn':
            $reference_data['bibcite_isbn'] = $ext_ref['external-id-value'];
            break;

          default:
            $reference_data['bibcite_other_number'] = $ext_ref['external-id-value'];
            break;
        }
      }
    }
    $encoded_works = json_encode($reference_data);
    if (!\Drupal::config('bibcite_import_orcid.settings')->get('orcid_sync_authors')) {
      $import = new Import();
      return $encoded_works ? $import->importOrcid($encoded_works, $uid, $context) : FALSE;
    }
    else {
      return $encoded_works;
    }
  }

  /**
   * Get the authors from ORCID work.
   *
   * @return array
   *   The authors.
   */
  public function getOrcidAuthors($work_data, $orcid, $bibtex_info = NULL) {
    $contribs = $work_data['contributors']['contributor'];
    if ($contribs && $contribs[0]["credit-name"]["value"]) {
      $contributors = array_map(function ($contrib) {
        return $contrib['credit-name']['value'];
      }, $contribs);
    }
    elseif ($bibtex_info) {
      $contribs = $bibtex_info['bibcite_authors'];
      foreach ($contribs as $contributor) {
        $first = $this->removeLatexAccents($contributor['first']);
        $last = $this->removeLatexAccents($contributor['last']);
        $contributors[] = $first . ' ' . $last;
      }
    }
    else {
      $person_data = $this->fetch->getUserFromOrcid($orcid);
      $contributors[] = $person_data["person"]["name"]["given-names"]["value"] . ' ' . $person_data["person"]["name"]["family-name"]["value"];
    }
    return $contributors;
  }

  /**
   * Get the authors from the Reference.
   *
   * @return array
   *   The contributors.
   */
  public function getReferenceAuthors($ref_id, $uid, $contributors) {
    $bibcite_reference = Reference::load($ref_id);
    $pub_refs = $bibcite_reference->author->referencedEntities();
    if (!empty($pub_refs)) {
      // Get the contributors name from the existing publication.
      $pub_contributors = [];
      foreach ($pub_refs as $referenced_contributor) {
        $key = $referenced_contributor->id();
        $pub_contributors[$key] = $referenced_contributor->getName();
      };
      // Look for linked authors.
      $linked_authors = [];
      foreach ($pub_contributors as $key => $contrib_id) {
        $query_users = \Drupal::entityQuery('user')
          ->accessCheck(FALSE)
          ->condition('field_author', $key)
          ->execute();
        if ($query_users) {
          $linked_authors[$key] = reset($query_users);
        }
      }
    }
    else {
      foreach ($contributors as $contributor) {
        $options[] = $contributor;
      }
    }

    // Check if the current user is already an author.
    foreach ($pub_contributors as $key => $contributor) {
      if (array_key_exists($key, $linked_authors)) {
        $attributes = [];
        // Is the onwer?
        if (in_array($uid, $linked_authors)) {
          $options[$key] = $contributor;
          $attributes[$key] = ['disabled' => 'disabled', 'readonly' => TRUE];
        }
        else {
          // Other linked users.
          $options[$key] = $contributor;
          $attributes[$key] = ['disabled' => 'disabled'];
        }
      }
      else {
        // Unlinked users.
        $options[$key] = $contributor;
      }
      // Are there authors missing.
      if ($pub_contributors && $contributors && count($pub_contributors) < count($contributors)) {
        $missing_contribs = array_diff($contributors, $pub_contributors);
        // Add an option for each contributor.
        foreach ($missing_contribs as $key => $contributor) {
          $options[$key] = $contributor;
        }
      }
    }
    return [$options, $attributes];
  }

  /**
   * Process bibtex citation if it exists.
   *
   * @return array
   *   The bibtex data.
   */
  public function parseBibtex($work) {
    if (!$this->validateBibtexSyntax($work)) {
      return [];
    }
    try {
      $parser = new Parser();
      $listener = new Listener();
      $listener->addProcessor(new TagNameCaseProcessor(CASE_LOWER));
      $listener->addProcessor(new NamesProcessor());
      $parser->addListener($listener);
      $parser->parseString($work);
      $reference_data = [];
      $entries = $listener->export();
      if (is_array($entries)) {
        foreach ($entries[0] as $prop => $value) {
          switch ($prop) {
            case 'pages':
            case 'Pages':
            case 'PAGES':
              $reference_data['bibcite_pages'] = $entries[0][$prop];
              break;

            case 'number':
            case 'Number':
            case 'NUMBER':
              $reference_data['bibcite_issue'] = $entries[0][$prop];
              break;

            case 'volume':
            case 'Volume':
            case 'VOLUME':
              $reference_data['bibcite_volume'] = $entries[0][$prop];
              break;

            case 'author':
            case 'Author':
            case 'AUTHOR':
              $reference_data['bibcite_authors'] = $entries[0][$prop];
              break;

            case 'booktitle':
            case 'Booktitle':
            case 'BookTitle':
            case 'BOOKTITLE':
              $reference_data['bibcite_secondary_title'] = $entries[0][$prop];
              break;

            case 'series':
            case 'Series':
            case 'SERIES':
              $reference_data['bibcite_section'] = $entries[0][$prop];
              break;

            case 'publisher':
            case 'Publisher':
            case 'PUBLISHER':
              $reference_data['bibcite_publisher'] = $entries[0][$prop];
              break;
          }
        }
      }
      return $reference_data;
    }
    catch (ProcessorException $e) {
      \Drupal::messenger()->addStatus(t('Bibtex Exception:') . ' ' . $e->getMessage() . ' ' . $work);
    }
  }

  /**
   * Checks if bibtex is valid.
   *
   * @return bool
   *   True if valid.
   */
  public function validateBibtexSyntax($input) {
    // Regex to match the beginning of a BibTeX entry.
    $bibtex_regex = '/@([a-zA-Z]+)\s*{\s*([^,]+),/';

    // Cyrilic is not supported by the parser.
    if (preg_match('/[\x{0400}-\x{04FF}]/u', $input)) {
      return FALSE;
    }

    // If string contains ?? return false.
    if (str_contains($input, '??')) {
      return FALSE;
    }

    // If string contains "" return false.
    if (str_contains($input, '""')) {
      return FALSE;
    }

    // Check if the BibTeX entry matches the regex pattern.
    if (!preg_match($bibtex_regex, $input, $matches)) {
      return FALSE;
    }

    // Check for spaces in the citation key (not allowed)
    if (isset($matches[2]) && strpos($matches[2], ' ') !== FALSE) {
      return FALSE;
    }

    // Check for balanced curly braces.
    $stack = [];
    for ($i = 0; $i < strlen($input); $i++) {
      $char = $input[$i];
      switch ($char) {
        case '{':
          array_push($stack, '{');
          break;

        case '}':
          if (empty($stack)) {
            return FALSE;
          }
          array_pop($stack);
          break;

        // No default case needed.
      }
    }

    // If the stack is not empty, there are unmatched braces.
    return empty($stack);
  }

  /**
   * Merge contributors.
   *
   * @param int $uid
   *   User id.
   *
   * @return void
   *   Return nothing.
   */
  public function mergeUserContributors($uid) {
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
    $cids = $user->field_author->getValue();
    $cids = array_column($cids, 'target_id');
    $cids = array_unique($cids);

    $bibcite_contributor = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    $contributors = [];
    foreach ($cids as $cid) {
      $contributor = $bibcite_contributor->load($cid);
      if ($contributor) {
        $similar_cids = \Drupal::entityQuery('bibcite_contributor')
          ->accessCheck(FALSE)
          ->condition('first_name', $contributor->first_name->value)
          ->condition('last_name', $contributor->last_name->value)
          ->condition('id', $cids, 'IN')
          ->sort('id', 'ASC')
          ->execute();
        $oldest_contrib_id = reset($similar_cids);
        foreach (array_slice($similar_cids, 1) as $cid) {
          $this->replaceAuthorId($cid, $oldest_contrib_id);
        }
        $contributors[$oldest_contrib_id] = $oldest_contrib_id;
      }

    }
    $user->field_author = $contributors;
    $user->save();
  }

  /**
   * Replace contributor id in references.
   *
   * @param int $old
   *   Old contributor id.
   * @param int $new
   *   New contributor id.
   *
   * @return void
   *   Return nothing.
   */
  public function replaceAuthorId($old, $new) {

    $referencesWithCid = \Drupal::entityQuery('bibcite_reference')
      ->accessCheck(FALSE)
      ->condition('author.target_id', $old)
      ->execute();
    if ($referencesWithCid) {
      foreach ($referencesWithCid as $entityId) {
        $entity = \Drupal::entityTypeManager()->getStorage('bibcite_reference')->load($entityId);
      }

      if ($entity) {
        $authors = $entity->get('author')->getValue();
        foreach ($authors as $key => $author) {
          if ($author['target_id'] == $old) {
            $authors[$key]['target_id'] = $new;
          }
        }
        $entity->set('author', $authors);
        $entity->save();
      }
    }
  }

  /**
   * Find ID from publication name, type and doi.
   *
   * @return int
   *   The id of the existing publication.
   */
  public function getExistingPublication($title, $type, $doi = NULL) {
    $query_reference = \Drupal::entityQuery('bibcite_reference')
      ->accessCheck(FALSE)
      ->condition('bibcite_doi', $doi)
      ->condition('type', $type)
      ->execute();

    if (empty($query_reference)) {
      // Check if the reference is already on bibcite.
      $query_reference = \Drupal::entityQuery('bibcite_reference')
        ->accessCheck(FALSE)
        ->condition('title', $title)
        ->condition('type', $type)
        ->execute();
    }
    return reset($query_reference);
  }

  /**
   * Remove latex accents.
   *
   * @return string
   *   The string without accents.
   */
  public function removeLatexAccents($inputStr) {
    $replacements =
      [
        '\"A' => 'Ä',
        '\"a' => 'ä',
        "\'A" => 'Á',
        "\'a" => 'á',
        '\.A' => 'Ȧ',
        '\.a' => 'ȧ',
        '\=A' => 'Ā',
        '\=a' => 'ā',
        '\^A' => 'Â',
        '\^a' => 'â',
        '\`A' => 'À',
        '\`a' => 'à',
        '\k{A}' => 'Ą',
        '\k{a}' => 'ą',
        '\r{A}' => 'Å',
        '\r{a}' => 'å',
        '\u{A}' => 'Ă',
        '\u{a}' => 'ă',
        '\v{A}' => 'Ǎ',
        '\v{a}' => 'ǎ',
        '\~A' => 'Ã',
        '\~a' => 'ã',
        "\'C" => 'Ć',
        "\'c" => 'ć',
        '\.C' => 'Ċ',
        '\.c' => 'ċ',
        '\^C' => 'Ĉ',
        '\^c' => 'ĉ',
        '\c{C}' => 'Ç',
        '\c{c}' => 'ç',
        '\v{C}' => 'Č',
        '\v{c}' => 'č',
        '\v{D}' => 'Ď',
        '\v{d}' => 'ď',
        '\"E' => 'Ë',
        '\"e' => 'ë',
        "\'E" => 'É',
        "\'e" => 'é',
        '\.E' => 'Ė',
        '\.e' => 'ė',
        '\=E' => 'Ē',
        '\=e' => 'ē',
        '\^E' => 'Ê',
        '\^e' => 'ê',
        '\`E' => 'È',
        '\`e' => 'è',
        '\c{E}' => 'Ȩ',
        '\c{e}' => 'ȩ',
        '\k{E}' => 'Ę',
        '\k{e}' => 'ę',
        '\u{E}' => 'Ĕ',
        '\u{e}' => 'ĕ',
        '\v{E}' => 'Ě',
        '\v{e}' => 'ě',
        '\.G' => 'Ġ',
        '\.g' => 'ġ',
        '\^G' => 'Ĝ',
        '\^g' => 'ĝ',
        '\c{G}' => 'Ģ',
        '\c{g}' => 'ģ',
        '\u{G}' => 'Ğ',
        '\u{g}' => 'ğ',
        '\v{G}' => 'Ǧ',
        '\v{g}' => 'ǧ',
        '\^H' => 'Ĥ',
        '\^h' => 'ĥ',
        '\v{H}' => 'Ȟ',
        '\v{h}' => 'ȟ',
        '\"I' => 'Ï',
        '\"i' => 'ï',
        "\'I" => 'Í',
        "\'i" => 'í',
        '\.I' => 'İ',
        '\=I' => 'Ī',
        '\=i' => 'ī',
        '\^I' => 'Î',
        '\^i' => 'î',
        '\`I' => 'Ì',
        '\`i' => 'ì',
        '\k{I}' => 'Į',
        '\k{i}' => 'į',
        '\u{I}' => 'Ĭ',
        '\u{i}' => 'ĭ',
        '\v{I}' => 'Ǐ',
        '\v{i}' => 'ǐ',
        '\~I' => 'Ĩ',
        '\~i' => 'ĩ',
        '\^J' => 'Ĵ',
        '\^j' => 'ĵ',
        '\c{K}' => 'Ķ',
        '\c{k}' => 'ķ',
        '\v{K}' => 'Ǩ',
        '\v{k}' => 'ǩ',
        "\'L" => 'Ĺ',
        "\'l" => 'ĺ',
        '\c{L}' => 'Ļ',
        '\c{l}' => 'ļ',
        '\v{L}' => 'Ľ',
        '\v{l}' => 'ľ',
        "\'N" => 'Ń',
        "\'n" => 'ń',
        '\c{N}' => 'Ņ',
        '\c{n}' => 'ņ',
        '\v{N}' => 'Ň',
        '\v{n}' => 'ň',
        '\~N' => 'Ñ',
        '\~n' => 'ñ',
        '\"O' => 'Ö',
        '\"o' => 'ö',
        "\'O" => 'Ó',
        "\'o" => 'ó',
        '\.O' => 'Ȯ',
        '\.o' => 'ȯ',
        '\=O' => 'Ō',
        '\=o' => 'ō',
        '\^O' => 'Ô',
        '\^o' => 'ô',
        '\`O' => 'Ò',
        '\`o' => 'ò',
        '\H{O}' => 'Ő',
        '\H{o}' => 'ő',
        '\k{O}' => 'Ǫ',
        '\k{o}' => 'ǫ',
        '\u{O}' => 'Ŏ',
        '\u{o}' => 'ŏ',
        '\v{O}' => 'Ǒ',
        '\v{o}' => 'ǒ',
        '\~O' => 'Õ',
        '\~o' => 'õ',
        "\'R" => 'Ŕ',
        "\'r" => 'ŕ',
        '\c{R}' => 'Ŗ',
        '\c{r}' => 'ŗ',
        '\v{R}' => 'Ř',
        '\v{r}' => 'ř',
        "\'S" => 'Ś',
        "\'s" => 'ś',
        '\^S' => 'Ŝ',
        '\^s' => 'ŝ',
        '\c{S}' => 'Ş',
        '\c{s}' => 'ş',
        '\v{S}' => 'Š',
        '\v{s}' => 'š',
        '\c{T}' => 'Ţ',
        '\c{t}' => 'ţ',
        '\v{T}' => 'Ť',
        '\v{t}' => 'ť',
        '\"U' => 'Ü',
        '\"u' => 'ü',
        "\'U" => 'Ú',
        "\'u" => 'ú',
        '\=U' => 'Ū',
        '\=u' => 'ū',
        '\^U' => 'Û',
        '\^u' => 'û',
        '\`U' => 'Ù',
        '\`u' => 'ù',
        '\H{U}' => 'Ű',
        '\H{u}' => 'ű',
        '\k{U}' => 'Ų',
        '\k{u}' => 'ų',
        '\r{U}' => 'Ů',
        '\r{u}' => 'ů',
        '\u{U}' => 'Ŭ',
        '\u{u}' => 'ŭ',
        '\v{U}' => 'Ǔ',
        '\v{u}' => 'ǔ',
        '\~U' => 'Ũ',
        '\~u' => 'ũ',
        '\^W' => 'Ŵ',
        '\^w' => 'ŵ',
        '\"Y' => 'Ÿ',
        '\"y' => 'ÿ',
        "\'Y" => 'Ý',
        "\'y" => 'ý',
        '\=Y' => 'Ȳ',
        '\=y' => 'ȳ',
        '\^Y' => 'Ŷ',
        '\^y' => 'ŷ',
        "\'Z" => 'Ź',
        "\'z" => 'ź',
        '\.Z' => 'Ż',
        '\.z' => 'ż',
        '\v{Z}' => 'Ž',
        '\v{z}' => 'ž',
      ];
    $replaced = strtr($inputStr, $replacements);
    // If there are still '{\' or '}' , remove them.
    $replaced = str_replace(['{', '}', '\\'], '', $replaced);
    return $replaced;
  }

}
