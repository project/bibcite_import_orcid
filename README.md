# Bibcite Import Orcid

The purpose of this Contrib module is to be able to import publications from ORCID public API into 'Bibliography & Citation' module.

## Instructions
Install with composer
Add the 'Orcid import block' to the /user page


## Dependencies

- 'Bibliography & Citation'
- 'Form Options Attributes'

## Tooling

There are 3 drush commands available to delete innformation:

- bibcite_import_orcid:delete_refs
- bibcite_import_orcid:delete_contribs
- bibcite_import_orcid:delete_all