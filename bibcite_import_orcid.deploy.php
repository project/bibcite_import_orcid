<?php

/**
 * @file
 * Contains bibcite_import_orcid.deploy.php.
 */

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Checks if the necessary fields are available and create them if not.
 */
function bibcite_import_orcid_deploy_add_fields() {
  $entity_type = 'user';
  $bundle = 'user';
  $managed_fields = [
    'field_references',
    'field_orcid',
    'field_author',
    'field_periodicity',
    'field_last_sync',
  ];
  foreach ($managed_fields as $field_name) {
    $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);
    if (!$field) {
      switch ($field_name) {
        case 'field_references':
          create_bibcite_publications_field();
          break;

        case 'field_orcid':
          create_orcid_field();
          break;

        case 'field_author':
          create_author_field();
          break;

        case 'field_periodicity':
          create_select_periodicity();
          break;

        case 'field_last_sync':
          create_last_sync_field();
          break;
      }
    }
  }

  // Set 'bibcite_import_orcid.settings' > 'orcid_sync_authors' to true.
  $config = \Drupal::configFactory()->getEditable('bibcite_import_orcid.settings');
  // If empty, set to true.
  if (empty($config->get('orcid_sync_authors'))) {
    $config->set('orcid_sync_authors', 1);
    $config->save();
  }
}

/**
 * Create the field_bibcite_publications field.
 */
function create_bibcite_publications_field() {
  $field_storage = FieldStorageConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_references',
    'type' => 'entity_reference',
    'settings' => [
      'target_type' => 'bibcite_reference',
    ],
    'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
  ]);
  $field_storage->save();

  $field = FieldConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_references',
    'bundle' => 'user',
    'label' => 'My Publications',
    'translatable' => FALSE,
    'settings' => [
      'handler_settings' => [
        'target_bundles' => NULL,
      ],
    ],
  ]);
  $field->save();
}

/**
 * Create the field_orcid field.
 */
function create_orcid_field() {
  $field_storage = FieldStorageConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_orcid',
    'type' => 'string',
    'cardinality' => 1,
  ]);
  $field_storage->save();

  $field = FieldConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_orcid',
    'bundle' => 'user',
    'label' => 'ORCID',
  ]);
  $field->save();
}

/**
 * Create the field_author field.
 */
function create_author_field() {
  $field_storage = FieldStorageConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_author',
    'type' => 'entity_reference',
    'settings' => [
      'target_type' => 'bibcite_contributor',
    ],
    'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
  ]);
  $field_storage->save();

  $field = FieldConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_author',
    'bundle' => 'user',
    'label' => 'Author',
    'translatable' => FALSE,
    'settings' => [
      'handler_settings' => [
        'target_bundles' => NULL,
      ],
    ],
  ]);
  $field->save();
}

/**
 * Get a field with sync periodicity.
 */
function create_select_periodicity() {
  $field_storage = FieldStorageConfig::create([
    'entity_type' => 'user',
    'status' => TRUE,
    'field_name' => 'field_periodicity',
    'type' => 'list_string',
    'cardinality' => 1,
    'settings' => [
      'allowed_values' => [
        'no' => 'Don\'t sync bio',
        'monthly' => '1st day of the month',
        '90days' => 'Every 90 days',
        'annual' => 'Annually',
      ],
    ],
  ]);
  $field_storage->save();

  $field = FieldConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_periodicity',
    'bundle' => 'user',
    'label' => 'Periodicity',
    'description' => 'Select the periodicity to sync your bio from ORCID',
    'required' => TRUE,
    'default_value' => [['value' => 'monthly']],
  ]);
  $field->save();
}

/**
 * Get a string field with last sync.
 */
function create_last_sync_field() {
  $field_storage = FieldStorageConfig::create([
    'entity_type' => 'user',
    'status' => TRUE,
    'field_name' => 'field_last_sync',
    'type' => 'string',
    'cardinality' => 1,
  ]);
  $field_storage->save();

  $field = FieldConfig::create([
    'entity_type' => 'user',
    'field_name' => 'field_last_sync',
    'bundle' => 'user',
    'label' => 'Last sync',
    'description' => 'Last sync date',
  ]);
  $field->save();
}
