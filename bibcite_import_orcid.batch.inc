<?php

/**
 * @file
 * Contains bibcite_import_orcid.batch.inc.
 */

use Drupal\bibcite_import_orcid\Import;
use Drupal\bibcite_import_orcid\Process;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Prepare Import Publications.
 */
function bibcite_prep_import_orcid_batch($work, $user, &$context) {
  $process = new Process();
  $context['results']['work'][] = $process->prepImportOrcid($work, $user, $context);
  $context['results']['import_uid'] = $user->id();
}

/**
 * Complete a batch process.
 *
 * @param bool $success
 *   A boolean indicating whether the batch has completed successfully.
 * @param array $results
 *   The value set in $context['results'] by callback_batch_operation().
 * @param array|bool $operations
 *   If $success is FALSE, contains the operations that remained unprocessed.
 */
function bibcite_prep_import_orcid_batch_finished($success, $results, $operations) {
  if ($success) {
    $id = time();
    foreach ($_SESSION as $key => $value) {
      if (strpos($key, 'works_data_') !== FALSE) {
        unset($_SESSION[$key]);
      }
    }
    $_SESSION['works_data_' . $id] = $results['work'];
    if (\Drupal::config('bibcite_import_orcid.settings')->get('orcid_sync_authors')) {
      return new RedirectResponse('/orcid-import/import/' . $id);
    }
    else {
      return new RedirectResponse('/orcid-import/success/' . $id);
    }
  }
  else {
    $message = t('An error occured while preparing the import from ORCID. Check the logs for more information.') . ' ' . $operations . ' ' . t('items to be processed.');
    $messenger = \Drupal::messenger();
    $messenger->addMessage($message);
  }
}

/**
 * Import Publications.
 */
function bibcite_import_orcid_batch($work, $uid, &$context) {
  $import = new Import();
  $import->importOrcid($work, $uid, $context);
  return $context['results']['work'][] = $work;
}

/**
 * Complete a batch process.
 *
 * @param bool $success
 *   A boolean indicating whether the batch has completed successfully.
 * @param array $results
 *   The value set in $context['results'] by callback_batch_operation().
 * @param array|bool $operations
 *   If $success is FALSE, contains the operations that remained unprocessed.
 */
function bibcite_import_orcid_batch_finished($success, $results, $operations) {

  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    if (\Drupal::config('bibcite_import_orcid.settings')->get('orcid_sync_authors')) {
      $process = new Process();
      $process->mergeUserContributors($_SESSION["uid"]);
    }

    $id = time();
    $_SESSION['results_orcid_' . $id] = $results;
    return new RedirectResponse('/orcid-import/success/' . $id);
  }
  else {
    $message = t('An error occured while importing.');
  }
  $messenger = \Drupal::messenger();
  $messenger->addMessage($message);
}
